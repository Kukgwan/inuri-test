//
//  MainVC.m
//  Inuri
//
//  Created by Kukgwan on 2015. 2. 8..
//  Copyright (c) 2015년 Kukgwan. All rights reserved.
//

#import "MainVC.h"
#import "MainView.h"

//Next Page
#import "NoteVC.h"
#import "NotiVC.h"
#import "AlbumVC.h"
#import "EventVC.h"
#import "FoodVC.h"
#import "AttendanceVC.h"
#import "SmartBusVC.h"
#import "DocumentVC.h"
#import "OthersVC.h"
//

@interface MainVC ()

@property (strong, nonatomic) MainView* mainView;

@end

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setMainView];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - View

- (void)setMainView {
    
    [self.navigationController setNavigationBarHidden:YES];
    
    UIImageView* bg = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [bg setImage:[UIImage imageNamed:@"bg_Img_visual"]];
    [self.view addSubview:bg];
    
    _mainView = [[[NSBundle mainBundle] loadNibNamed:@"MainView" owner:nil options:nil] lastObject];
    self.mainView.delegate = self;
    CGPoint center = self.view.center;
    [self.mainView setCenter:CGPointMake(center.x, self.mainView.frame.size.height/2+30.f)];
    [self.mainView setAlpha:0.f];
    [self.view addSubview:self.mainView];
    
    [UIView animateWithDuration:0.4f animations:^{
        [self.mainView setAlpha:1.f];
    }];
}

- (void)removeMainView {
    [self.mainView removeFromSuperview];
    [self.mainView setAlpha:1.f];
}

#pragma mark - Next Page

- (void)pushNextPage:(NSInteger)number {
    UIViewController* vc;

    if (number == 1)    vc = [[NoteVC alloc] init];
    else if (number == 2)    vc = [[NotiVC alloc] init];
    else if (number == 3)    vc = [[AlbumVC alloc] init];
    else if (number == 4)    vc = [[EventVC alloc] init];
    else if (number == 5)    vc = [[FoodVC alloc] init];
    else if (number == 6)    vc = [[AttendanceVC alloc] init];
    else if (number == 7)    vc = [[SmartBusVC alloc] init];
    else if (number == 8)    vc = [[DocumentVC alloc] init];
    else if (number == 9)    vc = [[OthersVC alloc] init];
    
    [self.navigationController pushViewController:vc animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
