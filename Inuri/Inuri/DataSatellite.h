//
//  DataSatellite.h
//  MudoKorea
//
//  Created by Kukgwan on 2015. 1. 28..
//  Copyright (c) 2015년 Kukgwan. All rights reserved.
//

//iPhone before :           320x640,
//                          640x960,
//iPhone 5s                 640x1136
//iPhone 6 :                750x1334
//iPhone 6 Plus :           1080x1920

#import <Foundation/Foundation.h>

#define ApsoluteURL @"http://118.39.27.13:9002"                                 //테스트 서버 주소

#pragma mark - DataSatellite Interface
@interface DataSatellite : NSObject <NSURLConnectionDataDelegate,NSURLConnectionDelegate,NSURLSessionDelegate>

+ (DataSatellite *)sharedObject;
+ (NSURLSession *)sharedSession;

+ (void)getSearchedData:(NSString*)boardURL withBodyString:(NSString *)bodyString;


+ (void)pushMainVC;

@end

