//
//  LoginView.m
//  Safety
//
//  Created by Kukgwan on 2015. 1. 29..
//  Copyright (c) 2015년 Kukgwan. All rights reserved.
//

#import "LoginView.h"

@implementation LoginView

- (void)drawRect:(CGRect)rect {
    [self.idView.layer setCornerRadius:5.f];
    [self.pwView.layer setCornerRadius:5.f];
    [self.login_Button.layer setCornerRadius:5.f];
    [self.searchId_Button.layer setCornerRadius:5.f];
    [self.searchPwd_Button.layer setCornerRadius:5.f];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.id_TextField resignFirstResponder];
    [self.pwd_TextField resignFirstResponder];
    return YES;
}

- (IBAction)saveIdButtonPressed:(id)sender {
    UIButton* button = (UIButton *)sender;
    [button setSelected:!button.selected];
//    if (button.selected) [button setBackgroundColor:[UIColor greenColor]];
//    else [button setBackgroundColor:[UIColor whiteColor]];
}

- (IBAction)loginButtonPressed:(id)sender {
    [self.delegate loginButtonPressed];
}

- (IBAction)searchIdButtonPressed:(id)sender {
    [self.delegate loginButtonPressed];
}

- (IBAction)searchPwdButtonPressed:(id)sender {
    [self.delegate loginButtonPressed];
}


@end
