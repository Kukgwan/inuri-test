//
//  DocumentVC.m
//  Inuri
//
//  Created by Kukgwan on 2015. 2. 8..
//  Copyright (c) 2015년 Kukgwan. All rights reserved.
//

#import "DocumentVC.h"

@interface DocumentVC ()

@end

@implementation DocumentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavigationBar];
}

#pragma mark - NavigationBar

- (void)setNavigationBar {
    //네비게이션바 셋팅
    self.title = @"문서함";
    UIButton* backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setFrame:CGRectMake(0.f, 0.f, 35.f, 35.f)];
    [backBtn setImage:[UIImage imageNamed:@"btn_list"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* backBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationItem setLeftBarButtonItem:backBtnItem];
}

#pragma mark - Button Action

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
