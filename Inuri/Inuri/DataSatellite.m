//
//  DataSatellite.m
//  MudoKorea
//
//  Created by Kukgwan on 2015. 1. 28..
//  Copyright (c) 2015년 Kukgwan. All rights reserved.
//

#import "DataSatellite.h"
#import "AppDelegate.h"

@implementation DataSatellite

+ (DataSatellite *)sharedObject {
    static DataSatellite* obj = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        obj = [[DataSatellite alloc]init];
    });
    return obj;
}


+ (NSURLSession *)sharedSession {
    
    static NSURLSession* mySession = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *myConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
//        myConfig.allowsCellularAccess = YES;
        mySession = [NSURLSession sessionWithConfiguration:myConfig delegate:nil delegateQueue: [NSOperationQueue mainQueue]];
    });
    return mySession;
}

+ (void)getSearchedData:(NSString*)boardURL withBodyString:(NSString *)bodyString {

    NSMutableURLRequest* reqA = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ApsoluteURL,boardURL]]];
    [reqA setHTTPMethod:@"POST"];
    reqA.HTTPBody = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"SearchData : Login, Body : %@", bodyString);
    NSLog(@"URL : %@", reqA.URL);
    
    
    NSURLSessionDataTask * sDataT = [[DataSatellite sharedSession] dataTaskWithRequest:reqA completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSLog(@"///");
        NSLog(@"Data Length : %ld", [data length]);
        NSLog(@"Data : %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
//        [array removeAllObjects];
//        [array addObjectsFromArray:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil]];
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//        });
    }];
    
    /*
     NSURLSessionDataTask * sDataT = [[DataSatellite sharedSession] dataTaskWithURL:[NSURL URLWithString:boardURL] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
     //NSLog(@"%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
     [array removeAllObjects];
     [array addObjectsFromArray:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil]];
     
     dispatch_async(dispatch_get_main_queue(), ^{
     [tblV reloadData];
     if ([vc respondsToSelector:@selector(searchDataLoadFinished)]) {
     [vc searchDataLoadFinished];
     }
     });
     
     }];
     */
    //모든 NSURLSession task들은 서스펜디드 모드로 만들어지므로 resume 해줘야 한다.
    [sDataT resume];
}

+ (void)pushMainVC {
    AppDelegate* app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app loadMainVC];
}

@end