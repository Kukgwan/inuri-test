//
//  LoginView.h
//  Safety
//
//  Created by Kukgwan on 2015. 1. 29..
//  Copyright (c) 2015년 Kukgwan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginVC.h"

@interface LoginView : UIView


@property (strong, nonatomic) IBOutlet UIView*               idView;
@property (strong, nonatomic) IBOutlet UIView*               pwView;
@property (strong, nonatomic) IBOutlet UITextField*          id_TextField;
@property (strong, nonatomic) IBOutlet UITextField*          pwd_TextField;

@property (strong, nonatomic) IBOutlet UIButton*             rememberId_Button;
@property (strong, nonatomic) IBOutlet UIButton*             login_Button;
@property (strong, nonatomic) IBOutlet UIButton*             searchId_Button;
@property (strong, nonatomic) IBOutlet UIButton*             searchPwd_Button;

@property (strong, nonatomic) LoginVC* delegate;

@end
