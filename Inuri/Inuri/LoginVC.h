//
//  LoginVC.h
//  Inuri
//
//  Created by Kukgwan on 2015. 2. 8..
//  Copyright (c) 2015년 Kukgwan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginVC : UIViewController

- (void)loginButtonPressed;
- (void)searchIdButtonPressed;
- (void)searchPwdButtonPressed;

@end

