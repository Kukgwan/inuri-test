//
//  LoginVC.m
//  Inuri
//
//  Created by Kukgwan on 2015. 2. 8..
//  Copyright (c) 2015년 Kukgwan. All rights reserved.
//

#import "LoginVC.h"
#import "LoginView.h"
#import "DataSatellite.h"

@interface LoginVC ()

@property (strong,nonatomic) LoginView* loginView;

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setGesture];
    [self setLoginView];
    
    NSString* body = [NSString stringWithFormat:@"userid=%@&password=%@", @"ZA001", @"gaonsoft!"];
//    [DataSatellite getSearchedData:@"/app/login.do" withBodyString:body];
}

#pragma mark - View

- (void)setLoginView {
    
    UIImageView* bg = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [bg setImage:[UIImage imageNamed:@"bg_Img_visual"]];
    [self.view addSubview:bg];
    
    _loginView = [[[NSBundle mainBundle] loadNibNamed:@"LoginView" owner:nil options:nil] lastObject];
    self.loginView.delegate = self;
    CGPoint center = self.view.center;
    [self.loginView setCenter:CGPointMake(center.x, self.loginView.frame.size.height/2+30.f)];
    [self.view addSubview:self.loginView];
}

- (void)removeLoginView {
    [self.loginView removeFromSuperview];
    [self.loginView setAlpha:1.f];
    [DataSatellite pushMainVC];
}



#pragma mark - Gesture

- (void)setGesture {
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
    [self.view addGestureRecognizer:tap];
}



#pragma mark - Button Action

- (void)loginButtonPressed {
    NSLog(@"loginButtonPressed");
    
    
    
    
    
    
    
    //로그인 동작 후
    [UIView animateWithDuration:0.4f animations:^{
        [self.loginView setAlpha:0.f];
    } completion:^(BOOL finishied) {
        [self removeLoginView];
    }];
    //
}

- (void)searchIdButtonPressed {
    NSLog(@"searchIdButtonPressed");
}

- (void)searchPwdButtonPressed {
    NSLog(@"searchPwdButtonPressed");
}



#pragma mark - TextFiled Return Action
- (void)tap {
    [self.loginView.id_TextField resignFirstResponder];
    [self.loginView.pwd_TextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
