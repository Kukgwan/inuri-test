//
//  MainView.h
//  Inuri
//
//  Created by Kukgwan on 2015. 2. 8..
//  Copyright (c) 2015년 Kukgwan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainVC.h"

@interface MainView : UIView

@property (strong, nonatomic) MainVC* delegate;

@end
