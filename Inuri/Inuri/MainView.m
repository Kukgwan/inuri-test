//
//  MainView.m
//  Inuri
//
//  Created by Kukgwan on 2015. 2. 8..
//  Copyright (c) 2015년 Kukgwan. All rights reserved.
//

#import "MainView.h"

@implementation MainView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)noteButtonPressed:(UIButton *)sender {
    [self.delegate pushNextPage:sender.tag];
}



@end
