//
//  main.m
//  Inuri
//
//  Created by Kukgwan on 2015. 2. 8..
//  Copyright (c) 2015년 Kukgwan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
